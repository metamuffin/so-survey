export type KResponseId = number
export type KConvertedCompYearly = number
export type KCompTotal = number
export type KMainBranch = "I am a developer by profession" | "I am a student who is learning to code" | "I am not primarily a developer, but I write code sometimes as part of my work" | "I code primarily as a hobby" | "I used to be a developer by profession, but no longer am" | "None of these"
export type KEmployment = "Independent contractor, freelancer, or self-employed" | "Student, full-time" | "Employed full-time" | "Student, part-time" | "I prefer not to say" | "Employed part-time" | "Not employed, but looking for work" | "Retired" | "Not employed, and not looking for work" | "NA"
export type KCountry = "Slovakia" | "Netherlands" | "Russian Federation" | "Austria" | "United Kingdom of Great Britain and Northern Ireland" | "United States of America" | "Malaysia" | "India" | "Sweden" | "Spain" | "Germany" | "Peru" | "Turkey" | "Canada" | "Singapore" | "Brazil" | "France" | "Switzerland" | "Malawi" | "Israel" | "Poland" | "Ukraine" | "Viet Nam" | "Portugal" | "Italy" | "Bulgaria" | "Greece" | "Iran, Islamic Republic of..." | "Ireland" | "Georgia" | "Uzbekistan" | "Hungary" | "Belgium" | "Pakistan" | "Nigeria" | "Albania" | "Bangladesh" | "Romania" | "Sri Lanka" | "Lithuania" | "Slovenia" | "Croatia" | "Czech Republic" | "Denmark" | "Armenia" | "Lebanon" | "Bahrain" | "Egypt" | "Nepal" | "Colombia" | "Indonesia" | "Australia" | "Turkmenistan" | "Morocco" | "Chile" | "Serbia" | "New Zealand" | "Estonia" | "Tunisia" | "Finland" | "Hong Kong (S.A.R.)" | "United Arab Emirates" | "Argentina" | "Azerbaijan" | "Philippines" | "Costa Rica" | "South Africa" | "Kosovo" | "Japan" | "United Republic of Tanzania" | "Bolivia" | "Bosnia and Herzegovina" | "Uruguay" | "South Korea" | "China" | "Norway" | "Belarus" | "Luxembourg" | "Malta" | "Ethiopia" | "Madagascar" | "Kenya" | "The former Yugoslav Republic of Macedonia" | "Botswana" | "Algeria" | "Senegal" | "Mexico" | "Cyprus" | "Venezuela, Bolivarian Republic of..." | "Cameroon" | "Jordan" | "Dominican Republic" | "Ecuador" | "Syrian Arab Republic" | "Zambia" | "Taiwan" | "Nomadic" | "Latvia" | "Guatemala" | "Paraguay" | "Iceland" | "Haiti" | "Republic of Moldova" | "Kazakhstan" | "Libyan Arab Jamahiriya" | "Afghanistan" | "Panama" | "Côte d'Ivoire" | "Cuba" | "Myanmar" | "Tajikistan" | "Lao People's Democratic Republic" | "Yemen" | "Thailand" | "Qatar" | "Democratic Republic of the Congo" | "Iraq" | "Mozambique" | "Somalia" | "Andorra" | "Kyrgyzstan" | "Kuwait" | "Saudi Arabia" | "Mauritania" | "Honduras" | "Angola" | "Oman" | "Swaziland" | "Sudan" | "Guyana" | "Chad" | "El Salvador" | "Benin" | "North Korea" | "Nicaragua" | "Dominica" | "Trinidad and Tobago" | "Ghana" | "Barbados" | "Burundi" | "Micronesia, Federated States of..." | "Zimbabwe" | "Mauritius" | "Gambia" | "Bahamas" | "Congo, Republic of the..." | "Suriname" | "Djibouti" | "Republic of Korea" | "Bhutan" | "Cambodia" | "Uganda" | "Rwanda" | "Montenegro" | "Maldives" | "Saint Kitts and Nevis" | "Monaco" | "Togo" | "Isle of Man" | "Jamaica" | "Belize" | "Palestine" | "Mongolia" | "Burkina Faso" | "Liechtenstein" | "Saint Lucia" | "Cape Verde" | "Brunei Darussalam" | "Namibia" | "Central African Republic" | "Lesotho" | "Guinea" | "Liberia" | "Fiji" | "Niger" | "Sierra Leone" | "San Marino" | "Saint Vincent and the Grenadines" | "Tuvalu" | "Papua New Guinea" | "Mali"
export type KUS_State = "NA" | "Georgia" | "New Hampshire" | "Missouri" | "District of Columbia" | "Massachusetts" | "Louisiana" | "Virginia" | "New York" | "Texas" | "Colorado" | "Minnesota" | "Maryland" | "California" | "Iowa" | "Illinois" | "Arkansas" | "South Carolina" | "Pennsylvania" | "Nebraska" | "North Carolina" | "Tennessee" | "Kansas" | "Connecticut" | "Rhode Island" | "Florida" | "Ohio" | "Vermont" | "Kentucky" | "Washington" | "New Jersey" | "Alabama" | "Wisconsin" | "Indiana" | "North Dakota" | "Utah" | "Arizona" | "Michigan" | "Oregon" | "Montana" | "Maine" | "West Virginia" | "Oklahoma" | "New Mexico" | "I do not reside in the United States" | "Idaho" | "South Dakota" | "Alaska" | "Nevada" | "Puerto Rico" | "Delaware" | "Hawaii" | "Wyoming" | "Mississippi" | "American Samoa" | "Guam"
export type KUK_Country = "NA" | "England" | "Scotland" | "Northern Ireland" | "Wales"
export type KEdLevel = "Secondary school (e.g. American high school, German Realschule or Gymnasium, etc.)" | "Bachelor’s degree (B.A., B.S., B.Eng., etc.)" | "Master’s degree (M.A., M.S., M.Eng., MBA, etc.)" | "Other doctoral degree (Ph.D., Ed.D., etc.)" | "Some college/university study without earning a degree" | "Something else" | "Professional degree (JD, MD, etc.)" | "Primary/elementary school" | "Associate degree (A.A., A.S., etc.)" | "NA"
export type KAge1stCode = "18 - 24 years" | "11 - 17 years" | "5 - 10 years" | "25 - 34 years" | "35 - 44 years" | "Younger than 5 years" | "45 - 54 years" | "55 - 64 years" | "NA" | "Older than 64 years"
export type KLearnCode = "Coding Bootcamp" | "Other online resources (ex: videos, blogs, etc)" | "School" | "Online Forum" | "NA" | "Friend or family member" | "Online Courses or Certification" | "Other (please specify):" | "Colleague" | "Books / Physical media"
export type KYearsCode = "NA" | "7" | "17" | "3" | "4" | "6" | "16" | "12" | "15" | "10" | "40" | "9" | "26" | "14" | "39" | "20" | "8" | "19" | "5" | "Less than 1 year" | "22" | "2" | "1" | "34" | "21" | "13" | "25" | "24" | "30" | "31" | "18" | "38" | "More than 50 years" | "27" | "41" | "42" | "35" | "23" | "28" | "11" | "37" | "44" | "43" | "36" | "33" | "45" | "29" | "50" | "46" | "32" | "47" | "49" | "48"
export type KYearsCodePro = "NA" | "10" | "4" | "5" | "6" | "2" | "30" | "9" | "18" | "12" | "21" | "1" | "16" | "Less than 1 year" | "15" | "3" | "35" | "7" | "8" | "17" | "14" | "26" | "25" | "20" | "50" | "34" | "11" | "24" | "22" | "13" | "31" | "23" | "39" | "41" | "27" | "28" | "19" | "33" | "More than 50 years" | "37" | "29" | "32" | "43" | "40" | "38" | "45" | "42" | "46" | "36" | "44" | "47" | "48" | "49"
export type KDevType = "Developer, mobile" | "NA" | "Developer, front-end" | "Developer, desktop or enterprise applications" | "Developer, full-stack" | "Engineer, data" | "Other (please specify):" | "Data scientist or machine learning specialist" | "Developer, back-end" | "Academic researcher" | "Database administrator" | "Scientist" | "Student" | "Developer, QA or test" | "Developer, game or graphics" | "Developer, embedded applications or devices" | "DevOps specialist" | "Data or business analyst" | "Designer" | "Engineering manager" | "Engineer, site reliability" | "System administrator" | "Product manager" | "Senior Executive (C-Suite, VP, etc.)" | "Educator" | "Marketing or sales professional"
export type KOrgSize = "20 to 99 employees" | "NA" | "100 to 499 employees" | "Just me - I am a freelancer, sole proprietor, etc." | "10,000 or more employees" | "10 to 19 employees" | "1,000 to 4,999 employees" | "500 to 999 employees" | "5,000 to 9,999 employees" | "2 to 9 employees" | "I don’t know"
export type KCurrency = "EUR European Euro" | "NA" | "GBP\tPound sterling" | "INR\tIndian rupee" | "SEK\tSwedish krona" | "TRY\tTurkish lira" | "CAD\tCanadian dollar" | "SGD\tSingapore dollar" | "CHF\tSwiss franc" | "RUB\tRussian ruble" | "ILS\tIsraeli new shekel" | "USD\tUnited States dollar" | "BRL\tBrazilian real" | "BGN\tBulgarian lev" | "AED United Arab Emirates dirham" | "PLN\tPolish zloty" | "UZS\tUzbekistani som" | "HUF\tHungarian forint" | "PKR\tPakistani rupee" | "ZMW Zambian kwacha" | "NGN\tNigerian naira" | "ALL\tAlbanian lek" | "BDT\tBangladeshi taka" | "IRR\tIranian rial" | "RON\tRomanian leu" | "HRK\tCroatian kuna" | "GEL\tGeorgian lari" | "DKK\tDanish krone" | "AMD\tArmenian dram" | "LBP\tLebanese pound" | "BHD\tBahraini dinar" | "EGP\tEgyptian pound" | "AUD\tAustralian dollar" | "CLP\tChilean peso" | "IDR\tIndonesian rupiah" | "RSD\tSerbian dinar" | "KRW\tSouth Korean won" | "HKD\tHong Kong dollar" | "NPR\tNepalese rupee" | "UAH\tUkrainian hryvnia" | "JPY\tJapanese yen" | "TZS\tTanzanian shilling" | "PEN\tPeruvian sol" | "BOB\tBolivian boliviano" | "CZK\tCzech koruna" | "ZAR\tSouth African rand" | "ARS\tArgentine peso" | "NOK\tNorwegian krone" | "VND\tVietnamese dong" | "MYR\tMalaysian ringgit" | "MGA\tMalagasy ariary" | "KES\tKenyan shilling" | "LKR\tSri Lankan rupee" | "TND\tTunisian dinar" | "BYN\tBelarusian ruble" | "COP\tColombian peso" | "MKD\tMacedonian denar" | "BWP\tBotswana pula" | "DZD\tAlgerian dinar" | "BAM\tBosnia and Herzegovina convertible mark" | "CNY\tChinese Yuan Renminbi" | "MXN\tMexican peso" | "MAD\tMoroccan dirham" | "JOD\tJordanian dinar" | "DOP\tDominican peso" | "THB\tThai baht" | "ETB\tEthiopian birr" | "XOF\tWest African CFA franc" | "PYG\tParaguayan guarani" | "ISK\tIcelandic krona" | "HTG\tHaitian gourde" | "TWD\tNew Taiwan dollar" | "GTQ\tGuatemalan quetzal" | "XAF\tCentral African CFA franc" | "KZT\tKazakhstani tenge" | "LYD\tLibyan dinar" | "CUP\tCuban peso" | "SYP\tSyrian pound" | "PHP\tPhilippine peso" | "XCD\tEast Caribbean dollar" | "LAK\tLao kip" | "YER\tYemeni rial" | "TOP\tTongan pa’anga" | "IQD\tIraqi dinar" | "CRC\tCosta Rican colon" | "MRU\tMauritanian ouguiya" | "AZN\tAzerbaijan manat" | "SAR\tSaudi Arabian riyal" | "AOA\tAngolan kwanza" | "SDG\tSudanese pound" | "GYD\tGuyanese dollar" | "XPF\tCFP franc" | "NZD\tNew Zealand dollar" | "UYU\tUruguayan peso" | "HNL\tHonduran lempira" | "NIO\tNicaraguan cordoba" | "MZN\tMozambican metical" | "TTD\tTrinidad and Tobago dollar" | "GHS\tGhanaian cedi" | "BBD\tBarbadian dollar" | "FJD\tFijian dollar" | "VES\tVenezuelan bolivar" | "UGX\tUgandan shilling" | "KWD\tKuwaiti dinar" | "GMD\tGambian dalasi" | "LSL\tLesotho loti" | "MMK\tMyanmar kyat" | "KGS\tKyrgyzstani som" | "AFN\tAfghan afghani" | "MUR\tMauritian rupee" | "SZL\tSwazi lilangeni" | "RWF\tRwandan franc" | "MDL\tMoldovan leu" | "TMT\tTurkmen manat" | "KYD\tCayman Islands dollar" | "KHR\tCambodian riel" | "IMP\tManx pound" | "QAR\tQatari riyal" | "BZD\tBelize dollar" | "ANG Netherlands Antillean guilder" | "MNT\tMongolian tugrik" | "TJS\tTajikistani somoni" | "OMR\tOmani rial" | "BIF\tBurundi franc" | "JMD\tJamaican dollar" | "NAD\tNamibian dollar" | "GNF\tGuinean franc" | "none\tCook Islands dollar" | "CDF\tCongolese franc" | "PGK\tPapua New Guinean kina" | "LRD\tLiberian dollar" | "MVR\tMaldivian rufiyaa" | "GIP\tGibraltar pound" | "BSD\tBahamian dollar" | "AWG\tAruban florin" | "DJF\tDjiboutian franc" | "SHP\tSaint Helena pound" | "MWK\tMalawian kwacha" | "BTN\tBhutanese ngultrum" | "SRD\tSurinamese dollar" | "VUV\tVanuatu vatu" | "CVE\tCape Verdean escudo" | "BMD\tBermudian dollar" | "GGP\tGuernsey Pound" | "KPW\tNorth Korean won" | "XDR\tSDR (Special Drawing Right)" | "ERN\tEritrean nakfa"
export type KCompFreq = "Monthly" | "NA" | "Yearly" | "Weekly"
export type KLanguageHaveWorkedWith = "C++" | "HTML/CSS" | "JavaScript" | "Objective-C" | "PHP" | "Swift" | "Python" | "Assembly" | "C" | "R" | "Rust" | "TypeScript" | "Bash/Shell" | "SQL" | "C#" | "Java" | "Node.js" | "PowerShell" | "Ruby" | "Perl" | "Matlab" | "Kotlin" | "Julia" | "Haskell" | "Delphi" | "Go" | "Scala" | "Dart" | "NA" | "VBA" | "Groovy" | "Clojure" | "APL" | "LISP" | "F#" | "Elixir" | "Erlang" | "Crystal" | "COBOL"
export type KLanguageWantToWorkWith = "Swift" | "NA" | "Julia" | "Python" | "Rust" | "JavaScript" | "TypeScript" | "Bash/Shell" | "HTML/CSS" | "SQL" | "C#" | "C++" | "Go" | "Java" | "Node.js" | "Objective-C" | "Perl" | "PHP" | "Ruby" | "Haskell" | "Clojure" | "C" | "APL" | "Kotlin" | "R" | "F#" | "Scala" | "Assembly" | "Dart" | "Elixir" | "PowerShell" | "VBA" | "Matlab" | "LISP" | "Erlang" | "Groovy" | "Crystal" | "Delphi" | "COBOL"
export type KDatabaseHaveWorkedWith = "PostgreSQL" | "SQLite" | "NA" | "Elasticsearch" | "Redis" | "MySQL" | "Microsoft SQL Server" | "Oracle" | "MongoDB" | "MariaDB" | "DynamoDB" | "Couchbase" | "Firebase" | "Cassandra" | "IBM DB2"
export type KDatabaseWantToWorkWith = "SQLite" | "NA" | "Cassandra" | "Elasticsearch" | "PostgreSQL" | "Redis" | "Firebase" | "IBM DB2" | "MariaDB" | "Microsoft SQL Server" | "MySQL" | "MongoDB" | "Couchbase" | "DynamoDB" | "Oracle"
export type KPlatformHaveWorkedWith = "NA" | "Heroku" | "Microsoft Azure" | "AWS" | "Google Cloud Platform" | "DigitalOcean" | "Oracle Cloud Infrastructure" | "IBM Cloud or Watson"
export type KPlatformWantToWorkWith = "NA" | "Heroku" | "AWS" | "Microsoft Azure" | "DigitalOcean" | "Google Cloud Platform" | "IBM Cloud or Watson" | "Oracle Cloud Infrastructure"
export type KWebframeHaveWorkedWith = "Laravel" | "Symfony" | "Angular" | "Flask" | "Vue.js" | "jQuery" | "Express" | "React.js" | "Angular.js" | "Ruby on Rails" | "Django" | "FastAPI" | "NA" | "ASP.NET Core " | "Spring" | "Svelte" | "ASP.NET" | "Gatsby" | "Drupal"
export type KWebframeWantToWorkWith = "NA" | "Flask" | "Angular" | "jQuery" | "Express" | "React.js" | "Ruby on Rails" | "Django" | "FastAPI" | "ASP.NET Core " | "Vue.js" | "Spring" | "Laravel" | "Svelte" | "Gatsby" | "ASP.NET" | "Angular.js" | "Symfony" | "Drupal"
export type KMiscTechHaveWorkedWith = "NA" | "Cordova" | "NumPy" | "Pandas" | "TensorFlow" | "Torch/PyTorch" | "Apache Spark" | "Hadoop" | "Keras" | ".NET Core / .NET 5" | "Qt" | "Flutter" | ".NET Framework" | "React Native"
export type KMiscTechWantToWorkWith = "NA" | "Keras" | "NumPy" | "Pandas" | "TensorFlow" | "Torch/PyTorch" | "Hadoop" | "Qt" | "React Native" | ".NET Core / .NET 5" | "Apache Spark" | ".NET Framework" | "Cordova" | "Flutter"
export type KToolsTechHaveWorkedWith = "NA" | "Docker" | "Git" | "Yarn" | "Kubernetes" | "Unity 3D" | "Terraform" | "Flow" | "Unreal Engine" | "Ansible" | "Deno" | "Puppet" | "Xamarin" | "Chef" | "Pulumi"
export type KToolsTechWantToWorkWith = "NA" | "Git" | "Docker" | "Kubernetes" | "Yarn" | "Unity 3D" | "Unreal Engine" | "Terraform" | "Ansible" | "Chef" | "Deno" | "Xamarin" | "Pulumi" | "Puppet" | "Flow"
export type KNEWCollabToolsHaveWorkedWith = "PHPStorm" | "Xcode" | "Android Studio" | "IntelliJ" | "Notepad++" | "PyCharm" | "IPython/Jupyter" | "RStudio" | "Sublime Text" | "Visual Studio Code" | "NA" | "Atom" | "Vim" | "Visual Studio" | "Eclipse" | "Emacs" | "Neovim" | "Rider" | "NetBeans" | "Webstorm" | "RubyMine" | "TextMate"
export type KNEWCollabToolsWantToWorkWith = "Atom" | "Xcode" | "NA" | "IPython/Jupyter" | "RStudio" | "Sublime Text" | "Visual Studio Code" | "Notepad++" | "PyCharm" | "Vim" | "Android Studio" | "Visual Studio" | "Emacs" | "Eclipse" | "Neovim" | "IntelliJ" | "Webstorm" | "NetBeans" | "Rider" | "PHPStorm" | "RubyMine" | "TextMate"
export type KOpSys = "MacOS" | "Windows" | "Linux-based" | "BSD" | "Other (please specify):" | "NA" | "Windows Subsystem for Linux (WSL)"
export type KNEWStuck = "Call a coworker or friend" | "Visit Stack Overflow" | "Go for a walk or other physical activity" | "Google it" | "Watch help / tutorial videos" | "Do other work and come back later" | "Meditate" | "Play games" | "Panic" | "Visit another developer community (please name):" | "NA" | "Other (please specify):"
export type KNEWSOSites = "Stack Overflow" | "Stack Exchange" | "Stack Overflow for Teams (private knowledge sharing & collaboration platform for companies)" | "NA" | "I have never visited Stack Overflow or the Stack Exchange network"
export type KSOVisitFreq = "Multiple times per day" | "Daily or almost daily" | "A few times per week" | "A few times per month or weekly" | "NA" | "Less than once per month or monthly"
export type KSOAccount = "Yes" | "Not sure/can't remember" | "No" | "NA"
export type KSOPartFreq = "A few times per month or weekly" | "Daily or almost daily" | "Multiple times per day" | "A few times per week" | "I have never participated in Q&A on Stack Overflow" | "Less than once per month or monthly" | "NA"
export type KSOComm = "Yes, definitely" | "Neutral" | "Yes, somewhat" | "No, not at all" | "No, not really" | "NA" | "Not sure"
export type KNEWOtherComms = "No" | "Yes" | "NA"
export type KAge = "25-34 years old" | "18-24 years old" | "35-44 years old" | "Prefer not to say" | "45-54 years old" | "Under 18 years old" | "65 years or older" | "55-64 years old" | "NA"
export type KGender = "Man" | "Prefer not to say" | "Woman" | "Non-binary, genderqueer, or gender non-conforming" | "Or, in your own words:" | "NA"
export type KTrans = "No" | "Prefer not to say" | "Yes" | "NA" | "Or, in your own words:"
export type KSexuality = "Straight / Heterosexual" | "Prefer not to say" | "NA" | "Bisexual" | "Prefer to self-describe:" | "Gay or Lesbian" | "Queer"
export type KEthnicity = "White or of European descent" | "Prefer not to say" | "I don't know" | "Multiracial" | "Southeast Asian" | "South Asian" | "Hispanic or Latino/a/x" | "Middle Eastern" | "NA" | "East Asian" | "Biracial" | "Or, in your own words:" | "Black or of African descent" | "Indigenous (such as Native American, Pacific Islander, or Indigenous Australian)"
export type KAccessibility = "None of the above" | "I am deaf / hard of hearing" | "NA" | "Prefer not to say" | "I am blind / have difficulty seeing" | "I am unable to / find it difficult to type" | "I am unable to / find it difficult to walk or stand without assistance" | "Or, in your own words:"
export type KMentalHealth = "None of the above" | "NA" | "I have a concentration and/or memory disorder (e.g. ADHD)" | "Prefer not to say" | "I have a mood or emotional disorder (e.g. depression, bipolar disorder)" | "I have an anxiety disorder" | "I have autism / an autism spectrum disorder (e.g. Asperger's)" | "Or, in your own words:"
export type KSurveyLength = "Appropriate in length" | "Too long" | "NA" | "Too short"
export type KSurveyEase = "Easy" | "Neither easy nor difficult" | "NA" | "Difficult"
export interface SurveyEntry {
    ResponseId: KResponseId
    MainBranch: KMainBranch
    Employment: KEmployment
    Country: KCountry
    US_State: KUS_State
    UK_Country: KUK_Country
    EdLevel: KEdLevel
    Age1stCode: KAge1stCode
    LearnCode: KLearnCode[]
    YearsCode: KYearsCode
    YearsCodePro: KYearsCodePro
    DevType: KDevType[]
    OrgSize: KOrgSize
    Currency: KCurrency
    CompTotal: KCompTotal
    CompFreq: KCompFreq
    LanguageHaveWorkedWith: KLanguageHaveWorkedWith[]
    LanguageWantToWorkWith: KLanguageWantToWorkWith[]
    DatabaseHaveWorkedWith: KDatabaseHaveWorkedWith[]
    DatabaseWantToWorkWith: KDatabaseWantToWorkWith[]
    PlatformHaveWorkedWith: KPlatformHaveWorkedWith[]
    PlatformWantToWorkWith: KPlatformWantToWorkWith[]
    WebframeHaveWorkedWith: KWebframeHaveWorkedWith[]
    WebframeWantToWorkWith: KWebframeWantToWorkWith[]
    MiscTechHaveWorkedWith: KMiscTechHaveWorkedWith[]
    MiscTechWantToWorkWith: KMiscTechWantToWorkWith[]
    ToolsTechHaveWorkedWith: KToolsTechHaveWorkedWith[]
    ToolsTechWantToWorkWith: KToolsTechWantToWorkWith[]
    NEWCollabToolsHaveWorkedWith: KNEWCollabToolsHaveWorkedWith[]
    NEWCollabToolsWantToWorkWith: KNEWCollabToolsWantToWorkWith[]
    OpSys: KOpSys
    NEWStuck: KNEWStuck[]
    NEWSOSites: KNEWSOSites[]
    SOVisitFreq: KSOVisitFreq
    SOAccount: KSOAccount
    SOPartFreq: KSOPartFreq
    SOComm: KSOComm
    NEWOtherComms: KNEWOtherComms
    Age: KAge
    Gender: KGender[]
    Trans: KTrans
    Sexuality: KSexuality[]
    Ethnicity: KEthnicity[]
    Accessibility: KAccessibility[]
    MentalHealth: KMentalHealth[]
    SurveyLength: KSurveyLength
    SurveyEase: KSurveyEase
    ConvertedCompYearly: KConvertedCompYearly
}
export function load_entry(e: Record<string, string>): SurveyEntry {
    return {
        ResponseId: parseFloat(e.ResponseId) as number,
        MainBranch: e.MainBranch as KMainBranch,
        Employment: e.Employment as KEmployment,
        Country: e.Country as KCountry,
        US_State: e.US_State as KUS_State,
        UK_Country: e.UK_Country as KUK_Country,
        EdLevel: e.EdLevel as KEdLevel,
        Age1stCode: e.Age1stCode as KAge1stCode,
        LearnCode: e.LearnCode.split(";") as KLearnCode[],
        YearsCode: e.YearsCode as KYearsCode,
        YearsCodePro: e.YearsCodePro as KYearsCodePro,
        DevType: e.DevType.split(";") as KDevType[],
        OrgSize: e.OrgSize as KOrgSize,
        Currency: e.Currency as KCurrency,
        CompTotal: parseFloat(e.CompTotal) as number,
        CompFreq: e.CompFreq as KCompFreq,
        LanguageHaveWorkedWith: e.LanguageHaveWorkedWith.split(";") as KLanguageHaveWorkedWith[],
        LanguageWantToWorkWith: e.LanguageWantToWorkWith.split(";") as KLanguageWantToWorkWith[],
        DatabaseHaveWorkedWith: e.DatabaseHaveWorkedWith.split(";") as KDatabaseHaveWorkedWith[],
        DatabaseWantToWorkWith: e.DatabaseWantToWorkWith.split(";") as KDatabaseWantToWorkWith[],
        PlatformHaveWorkedWith: e.PlatformHaveWorkedWith.split(";") as KPlatformHaveWorkedWith[],
        PlatformWantToWorkWith: e.PlatformWantToWorkWith.split(";") as KPlatformWantToWorkWith[],
        WebframeHaveWorkedWith: e.WebframeHaveWorkedWith.split(";") as KWebframeHaveWorkedWith[],
        WebframeWantToWorkWith: e.WebframeWantToWorkWith.split(";") as KWebframeWantToWorkWith[],
        MiscTechHaveWorkedWith: e.MiscTechHaveWorkedWith.split(";") as KMiscTechHaveWorkedWith[],
        MiscTechWantToWorkWith: e.MiscTechWantToWorkWith.split(";") as KMiscTechWantToWorkWith[],
        ToolsTechHaveWorkedWith: e.ToolsTechHaveWorkedWith.split(";") as KToolsTechHaveWorkedWith[],
        ToolsTechWantToWorkWith: e.ToolsTechWantToWorkWith.split(";") as KToolsTechWantToWorkWith[],
        NEWCollabToolsHaveWorkedWith: e.NEWCollabToolsHaveWorkedWith.split(";") as KNEWCollabToolsHaveWorkedWith[],
        NEWCollabToolsWantToWorkWith: e.NEWCollabToolsWantToWorkWith.split(";") as KNEWCollabToolsWantToWorkWith[],
        OpSys: e.OpSys as KOpSys,
        NEWStuck: e.NEWStuck.split(";") as KNEWStuck[],
        NEWSOSites: e.NEWSOSites.split(";") as KNEWSOSites[],
        SOVisitFreq: e.SOVisitFreq as KSOVisitFreq,
        SOAccount: e.SOAccount as KSOAccount,
        SOPartFreq: e.SOPartFreq as KSOPartFreq,
        SOComm: e.SOComm as KSOComm,
        NEWOtherComms: e.NEWOtherComms as KNEWOtherComms,
        Age: e.Age as KAge,
        Gender: e.Gender.split(";") as KGender[],
        Trans: e.Trans as KTrans,
        Sexuality: e.Sexuality.split(";") as KSexuality[],
        Ethnicity: e.Ethnicity.split(";") as KEthnicity[],
        Accessibility: e.Accessibility.split(";") as KAccessibility[],
        MentalHealth: e.MentalHealth.split(";") as KMentalHealth[],
        SurveyLength: e.SurveyLength as KSurveyLength,
        SurveyEase: e.SurveyEase as KSurveyEase,
        ConvertedCompYearly: parseFloat(e.ConvertedCompYearly) as number,
    }
}
