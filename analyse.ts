import { load_entry, SurveyEntry } from "./schema.ts";

const data = (JSON.parse(
    await Deno.readTextFile("./data/survey.json"),
) as Record<string, string>[]).map(e => load_entry(e));

export function compare_two(
    x_fn: (e: SurveyEntry) => boolean,
    y_fn: (e: SurveyEntry) => boolean,
) {
    let count_a = 0;
    let count_x = 0;
    let count_y = 0;
    let count_x_and_y = 0;

    for (const e of data) {
        const x = x_fn(e);
        const y = y_fn(e);
        count_a += 1;
        if (x) count_x += 1;
        if (y) count_y += 1;
        if (x && y) count_x_and_y += 1;
    }

    function stat(v: number, per: number) {
        return (v / per * 100).toFixed(2) + "%" + `\t (${v} of ${per})`;
    }

    console.log("-----------------");
    console.log("```");
    console.log(`X: ${x_fn}`);
    console.log(`Y: ${y_fn}`);
    console.log("  |X| / |A|:      ", stat(count_x, count_a));
    console.log("  |Y| / |A|:      ", stat(count_y, count_a));
    console.log("  |X ∩ Y| / |X|:  ", stat(count_x_and_y, count_x));
    console.log("  |X ∩ Y| / |Y|:  ", stat(count_x_and_y, count_y));
    console.log("```");
}

export function compare_one_multi(
    x_fn: (e: SurveyEntry) => boolean,
    y_key: keyof SurveyEntry
) {
    let count_a = 0;
    let count_x = 0;
    const count_y = new Map<string, number>();
    const count_y_with_x = new Map<string, number>();

    for (const e of data) {
        const x = x_fn(e);
        const y = e[y_key];
        count_a += 1;
        if (x) count_x += 1;
        get_values(y).forEach(v => count_y.set(v, (count_y.get(v) ?? 0) + 1))
        if (x) get_values(y).forEach(v => count_y_with_x.set(v, (count_y_with_x.get(v) ?? 0) + 1))
    }

    function stat(ky: string, vy: number, vyx: number) {
        const p = (x: number) => (x * 100).toFixed(2) + "%"
        return ky.padEnd(max_len_key) + `: ${p(vy / count_a)}\t${p(vyx / count_x)}\t${p(vyx / vy)}`
    }

    console.log("-----------------");
    console.log("```");
    console.log(`X: ${x_fn}`);
    console.log(`for Y:`);

    const cy = [...count_y.keys()]
    const max_len_key = [...count_y.keys()].reduce((a, v) => Math.max(a, v.length), 0)
    console.log(" ".repeat(max_len_key), `Y/A; \tX∩Y/X; \tX∩Y/Y`);

    for (const ky of cy) {
        console.log(stat(ky, count_y.get(ky) ?? 0, count_y_with_x.get(ky) ?? 0))
    }

    console.log("```");
}


export function get_values(x: string | string[] | number): string[] {
    if (x instanceof Array) return x
    if (typeof x == "number") return [x.toString()]
    if (typeof x == "string") return [x]
    throw new Error("a");
}