import { readCSVObjects } from "https://deno.land/x/csv@v0.7.4/mod.ts";

const f = await Deno.open("./data/survey_results_public.csv");
const keys_multi: Set<string> = new Set()
const keys: Set<string> = new Set()
const possible_values = new Map<string, Set<string>>()

function madd(key: string, value: string) {
    const set = possible_values.get(key) ?? (() => {
        const s = new Set<string>()
        possible_values.set(key, s)
        return s
    })()
    set.add(value)
}

const key_overrides = new Map(Object.entries({
    ResponseId: "number",
    ConvertedCompYearly: "number",
    CompTotal: "number",
}))


let sample: Record<string, string> = {}
for await (const obj of readCSVObjects(f)) {
    const e = obj as unknown as Record<string, string>
    if (!sample) sample = { ...e }

    // for (const key of key_overrides.keys()) delete e[key]

    for (const [key, values_d] of Object.entries(e)) {
        keys.add(key)
        if (key_overrides.has(key)) continue
        const values = values_d.split(";")
        if (values.length > 1) keys_multi.add(key)
        for (const value of values) madd(key, value)
    }
}

f.close();


for (const [k, v] of key_overrides) {
    console.log(`export type K${k} = ${v}`);
}
for (const [k, v] of possible_values.entries()) {
    console.log(`export type K${k} = ${[...v.values()].map(v => JSON.stringify(v)).join(" | ")}`);
}

console.log(`export interface SurveyEntry {`);
for (const key of keys) {
    console.log(`    ${key}: K${key}${keys_multi.has(key) ? "[]" : ""}`);
}
console.log(`}`);

console.log(`export function load_entry(e: Record<string, string>): SurveyEntry {`);
console.log(`    return {`);
for (const key of keys) {
    let l = `e.${key}`
    if (keys_multi.has(key)) l += `.split(";")`
    if (key_overrides.get(key) == "number") l = `parseFloat(${l})`
    if (possible_values.has(key)) l += ` as K${key}`
    if (key_overrides.has(key)) l += ` as ${key_overrides.get(key)}`
    if (keys_multi.has(key)) l += `[]`
    console.log(`        ${key}: ${l},`);
}
console.log(`    }`);
console.log(`}`);


