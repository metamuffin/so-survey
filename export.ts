import { readCSVObjects } from "https://deno.land/x/csv@v0.7.4/mod.ts";
import { SurveyEntry } from "./schema.ts"

const f = await Deno.open("./data/survey_results_public.csv");

const entries = []

for await (const obj of readCSVObjects(f)) {
    const e = obj as unknown as SurveyEntry
    entries.push(e)
}

console.log(JSON.stringify(entries))
