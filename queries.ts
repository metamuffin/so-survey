import { compare_two } from "./analyse.ts";

compare_two(
    e => e.EdLevel == "Bachelor’s degree (B.A., B.S., B.Eng., etc.)",
    e => e.Country == "Germany",
)

compare_two(
    e => e.ConvertedCompYearly > 50 * 1000,
    e => e.EdLevel == "Bachelor’s degree (B.A., B.S., B.Eng., etc.)"
        || e.EdLevel == "Other doctoral degree (Ph.D., Ed.D., etc.)"
        || e.EdLevel == "Master’s degree (M.A., M.S., M.Eng., MBA, etc.)"
        || e.EdLevel == "Associate degree (A.A., A.S., etc.)"
)

compare_two(
    e => e.LanguageHaveWorkedWith.includes("Rust"),
    e => e.Sexuality.includes("Gay or Lesbian")
)

compare_two(
    e => e.LanguageHaveWorkedWith.includes("JavaScript") || e.LanguageHaveWorkedWith.includes("TypeScript"),
    e => e.LanguageHaveWorkedWith.includes("Rust")
)
